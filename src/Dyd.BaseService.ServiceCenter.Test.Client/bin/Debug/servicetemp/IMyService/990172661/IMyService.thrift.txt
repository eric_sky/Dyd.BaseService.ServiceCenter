

# Thrift支持的数据类型
# bool（bool）: 布尔类型(TRUE or FALSE)
# byte（byte）: 8位带符号整数
# i16（short）: 16位带符号整数
# i32（int）: 32位带符号整数
# i64（long）: 64位带符号整数
# double（double）: 64位浮点数
# string（string）: 采用UTF-8编码的字符串
# binary（byte[]）：未经过编码的字节流
# list(List<>)：
# map（Dictionary<,>）：

namespace csharp Dyd.BaseService.ServiceCenter.Test.Service.Thrift
namespace java dyd.baseservice.servicecenter.test.service.thrift


/** 类:测试实体,描述:我的测试实体 */
struct MyEntity1 {   
    
    /** 属性:实体参数1,描述:我的测试实体参数1 */
    1:i32 p1

  }
/** 类:测试实体2,描述:我的测试实体2 */
struct MyEntity2 {   
    
    /** 属性:List<int>,描述:List<int>参数 */
    1:list<i32> P2

    /** 属性:List<byte>,描述:List<byte>参数 */
    2:list<byte> P3

    /** 属性:Dictionary<string,string>,描述:Dictionary<string,string>参数 */
    3:map<string,string> P4

    /** 属性:bool,描述:bool参数 */
    4:bool P5

    /** 属性:long,描述:long参数 */
    5:i64 P6

    /** 属性:short,描述:short参数 */
    6:i16 P7

    /** 属性:Int16,描述:Int16参数 */
    7:i16 P8

    /** 属性:byte[],描述:byte[]参数 */
    8:binary P9

    /** 属性:String,描述:String参数 */
    9:string P10

  }

/** 服务:测试服务,描述:我的测试服务*/
service IMyServiceThrift {  
    
    /** 方法:测试方法,描述:我的测试方法*/
    string Test()

    /** 方法:测试实体,描述:我的测试的实体方法*/
    MyEntity1 ToDo(1:MyEntity1 entity)

    /** 方法:测试实体,描述:我的测试的实体方法*/
    MyEntity2 ToDo2(1:MyEntity2 entity)

    /** 方法:测试实体,描述:我的测试的实体方法*/
    bool ToDo3(1:i32 entity)

}

