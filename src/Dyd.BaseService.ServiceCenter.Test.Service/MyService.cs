﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    public class MyService:IMyService
    {
        public string Test() { return "Ok"; }
        public MyEntity1 ToDo(MyEntity1 entity)
        {
            return entity;
        }

        public MyEntity2 ToDo2(MyEntity2 entity)
        {
            return entity;
        }

        public bool ToDo3(int entity) { return true; }
    }
}
