﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Controllers
{
    [Authorize]
    public class SystemConfigController : Controller
    {

        #region List
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search">查询实体</param>
        /// <param name="pno">页码</param>
        /// <param name="pagesize">页大小</param>
        /// <returns></returns>
        public ActionResult SystemConfigIndex(tb_system_config_Search search)
        {
            int total = 0;
            IList<tb_system_config> list = new List<tb_system_config>();
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                list = tb_system_config_bll.Instance.GetPageList(conn, search, out total);
                var pagelist = new SPagedList<tb_system_config>(list, search.Pno, search.PageSize, total);

                if (Request.IsAjaxRequest())
                {
                    return PartialView("_SystemConfigIndex", pagelist);
                }
                else
                {
                    return View(pagelist);
                }
            }
        }
        #endregion

        #region Create
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult SystemConfigCreate()
        {
            return View();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SystemConfigCreate(tb_system_config model)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_system_config_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "配置重复！" });
                    }
                    var id = tb_system_config_bll.Instance.Add(conn, model);
                    tb_log_bll.Instance.Add(conn, string.Format("{0}|Id:{1}|Key:{2}|Value:{3}", "添加系统配置", id, model.key, model.value));

                    return Json(new { Flag = true, Message = "添加成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// 修改配置
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult SystemConfigEdit(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                var model = tb_system_config_bll.Instance.Get(conn, id);
                return View(model);
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SystemConfigEdit(tb_system_config model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_system_config_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "配置重复！" });
                    }

                    tb_system_config_bll.Instance.Update(conn, model);
                    tb_log_bll.Instance.Add(conn, string.Format("{0}|Id:{1}|Key:{2}|Value:{3}", "添加系统配置", model.id, model.key, model.value));
                    return Json(new { Flag = true, Message = "修改成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SystemConfigDelete(int id)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    var model = tb_system_config_bll.Instance.Get(conn, id);
                    tb_system_config_bll.Instance.Delete(conn, id);
                    tb_log_bll.Instance.Add(conn, string.Format("{0}|Id:{1}|Key:{2}|Value:{3}", "删除系统配置", model.id, model.key, model.value));
                    return Json(new { Flag = true, Message = "删除成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }
        #endregion
    }
}