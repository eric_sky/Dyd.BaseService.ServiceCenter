﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using Dyd.BaseService.ServiceCenter.Web.Core;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Controllers
{
    [Authorize]
    public class ProtocolVersionController : Controller
    {
        #region SetDefaultProtocolVersion
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="nodeId">nodeId</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetDefaultProtocolVersion([Bind(Prefix = "id")] int nodeId)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    tb_service_bll.Instance.SetDefaultProtocolVersion(conn, nodeId);
                    return Json(new { Flag = true, Message = "添加成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion



        #region List
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search">查询实体</param>
        /// <param name="pno">页码</param>
        /// <param name="pagesize">页大小</param>
        /// <returns></returns>
        public ActionResult ProtocolVersionIndex(tb_protocolversion_search search)
        {
            ViewBag.ServiceId = search.serviceid;
            int total = 0;
            IList<tb_protocolversion> list = new List<tb_protocolversion>();
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                list = tb_protocolversion_bll.Instance.GetPageList(conn, search, out total);
                var pagelist = new SPagedList<tb_protocolversion>(list, search.Pno, search.PageSize, total);

                if (Request.IsAjaxRequest())
                {
                    return PartialView("_ProtocolVersionIndex", pagelist);
                }
                else
                {
                    return View(pagelist);
                }
            }
        }
        #endregion

        #region Create
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult ProtocolVersionIndexCreate()
        {
            return View();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProtocolVersionIndexCreate(tb_protocolversion model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_protocolversion_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "！" });
                    }
                    tb_protocolversion_bll.Instance.Add(conn, model);
                    return Json(new { Flag = true, Message = "添加成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// 修改配置
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult ProtocolVersionIndexEdit(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                var model = tb_protocolversion_bll.Instance.Get(conn, id);
                return View(model);
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProtocolVersionIndexEdit(tb_protocolversion model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_protocolversion_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "！" });
                    }
                    tb_protocolversion_bll.Instance.Update(conn, model);
                    return Json(new { Flag = true, Message = "！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProtocolVersionIndexDelete(int id)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    tb_protocolversion_bll.Instance.Delete(conn, id);
                    return Json(new { Flag = true, Message = "删除成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }
        #endregion

        #region ProtocolVersionDown
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public ActionResult ProtocolVersionDown(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                var dt = DateTime.Now.ToString("yyyyMMddHHmmss");
                var protocolVersionModel = tb_protocolversion_bll.Instance.Get(conn, id);
                var serviceModel = tb_service_bll.Instance.Get(conn, protocolVersionModel.serviceid);
                string path = AppDomain.CurrentDomain.BaseDirectory.Trim('\\') + "\\ServiceSDKs\\" + dt + "\\";

                XXF.Common.IOHelper.CreateDirectory(path);
                XXF.BaseService.ServiceCenter.Client.ClientSDKHelper.ToLocalFile(serviceModel.servicenamespace, path);

                var fileName = serviceModel.servicename + dt + ".zip";
                CompressHelper.Compress(path, fileName);

                return Json(new { Url = "/ServiceSDKs/" + dt + "/" + fileName }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}