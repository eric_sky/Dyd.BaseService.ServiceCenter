﻿using Dyd.BaseService.ServiceCenter.Domain.Dal;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using XXF.Cache;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Domain.Bll
{
    //tb_client
    public partial class tb_client_bll
    {
        #region Init
        public static tb_client_bll Instance = new tb_client_bll();
        private readonly tb_client_dal dal = new tb_client_dal();
        private tb_client_bll()
        { }
        #endregion
        #region C
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_client model)
        {
            return dal.Add(conn, model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_client model)
        {
            return dal.Update(conn, model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            return dal.Delete(conn, id);
        }
        #endregion

        #region Q
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool IsExists(DbConn conn, tb_client model)
        {
            return dal.IsExists(conn, model);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_client Get(DbConn conn, int id)
        {
            return dal.Get(conn, id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public tb_client GetByCache(DbConn conn, int id)
        {
            string CacheKey = "tb_clientModel-" + id;
            var model = CacheHelper.GetCache(CacheKey + id, TimeSpan.FromMinutes(1), () => dal.Get(conn, id));
            return (tb_client)model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<tb_client> GetPageList(DbConn conn, tb_client_search search, out int totalCount)
        {
            return dal.GetPageList(conn, search, out  totalCount);
        }
        #endregion

    }
}