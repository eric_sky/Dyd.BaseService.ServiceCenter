﻿using Dyd.BaseService.ServiceCenter.Domain.Dal;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using XXF.Cache;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Domain.Bll
{
    //tb_error
    public partial class tb_error_bll
    {
        #region Init
        public static tb_error_bll Instance = new tb_error_bll();
        private readonly tb_error_dal dal = new tb_error_dal();
        private tb_error_bll()
        { }
        #endregion
        #region C
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_error model)
        {
            return dal.Add(conn, model);

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, string mes)
        {
            tb_error model = new tb_error();
            model.msg = mes;
            model.createtime = DateTime.Now;
            model.logtype = (int)XXF.BaseService.ServiceCenter.SystemRuntime.EnumLogType.System;
            return dal.Add(conn, model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_error model)
        {
            return dal.Update(conn, model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            return dal.Delete(conn, id);
        }

        public bool Clear(DbConn PubConn)
        {
            return dal.Clear(PubConn);
        }
        #endregion

        #region Q
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool IsExists(DbConn conn, tb_error model)
        {
            return dal.IsExists(conn, model);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_error Get(DbConn conn, int id)
        {
            return dal.Get(conn, id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public tb_error GetByCache(DbConn conn, int id)
        {
            string CacheKey = "tb_errorModel-" + id;
            var model = CacheHelper.GetCache(CacheKey + id, TimeSpan.FromMinutes(1), () => dal.Get(conn, id));
            return (tb_error)model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<tb_error> GetPageList(DbConn conn, tb_error_search search, out int totalCount)
        {
            return dal.GetPageList(conn, search, out  totalCount);
        }
        #endregion

    }
}