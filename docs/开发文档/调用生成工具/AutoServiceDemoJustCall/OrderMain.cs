
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoServiceDemoJustCall
{
    /// <summary>
    /// 【类】测试实体1,【描述】我的测试实体1
    /// </summary> 
    public class OrderMain 
    {
        
        /// <summary>
        /// 【属性】List,【描述】List
        /// </summary>     
        public List<SubOrderMain> SubOrderMain {get;set;}

        /// <summary>
        /// 【属性】Dictionary,【描述】Dictionary
        /// </summary>     
        public Dictionary<string,SubOrderMain> DSubOrderMain {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public long tid {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string cjsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shzh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shshsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shshmc {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shlxsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shlxdh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string yhzh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public long yhid {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string yhm {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shrxm {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shrsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shrsf {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shrcs {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shrdq {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shrdz {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public byte fkfs {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string fkzh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string zftid {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public byte fkzt {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double yf {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double zje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double yhqxfje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int yhqxfzs {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public byte sfdksh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string ddbz {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string dtjd {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string dtwd {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int ddzt {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int qrsh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int sfdy {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string qxly {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string fksj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public byte ckzt {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string jdsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shygsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shygxm {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string qrshsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double pjjg {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string pjnr {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string pjsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string ddxtbs {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string shddbz {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int shddbj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string ddkfbz {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string zhxgsj {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double yhje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string mjxx {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double dkje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double ysje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double fkfsje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double yezfje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double yfje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double sjjsje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double spje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double ddje {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int sfzt {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public long dayid {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public string soundurl {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public int soundtime {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public bool sfycsh {get;set;}

        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public double ddfwf {get;set;}

    }
}