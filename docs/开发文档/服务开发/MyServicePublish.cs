﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service;
using XXF.BaseService.ServiceCenter.Service.Provider;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Demo.Service
{
    public class MyServicePublish
    {
        public void Run()
        {
            /*
             * 0.到配置中心配置"ServiceCenterConnectString" 服务中心连接
             * 1.通过管理员至"服务中心"定义即将发布的服务的命名空间和协议类型等。
             * 2.将服务发布到"服务中心"（可以挂载到任务中心发布服务,也可以使用winservice）
             */
            //一句代码，发布服务。
           ServicePublishHelper.Publish<MyService>(typeof(IMyService),()=>{ System.Diagnostics.Debug.WriteLine("服务打开成功!这是服务打开成功后的回调");});
        }
    }
}
